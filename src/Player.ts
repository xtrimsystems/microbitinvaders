namespace Player
{
	import Coordinates = Helpers.Coordinates;

	const DEFAULT_RESISTANCE = 3;

	export class Spaceship
	{
		private resistance: number;
		private coordinates: Coordinates;
		private readonly color: ZipLedColors;

		public constructor(coordinates: Coordinates, color: ZipLedColors = ZipLedColors.Green)
		{
			this.resistance = DEFAULT_RESISTANCE;
			this.coordinates = coordinates;
			this.color = color;
		}

		public moveLeft(minX: number): boolean
		{
			let x = this.coordinates.getX();

			if (x > minX)
			{
				this.coordinates = new Coordinates(x - 1, this.coordinates.getY());

				return true;
			}

			return false;
		}

		public moveRight(maxX: number): boolean
		{
			let x = this.coordinates.getX();

			if (x < maxX)
			{
				this.coordinates = new Coordinates(x + 1, this.coordinates.getY());

				return true;
			}

			return false;
		}

		public receiveHit(hits: number): void
		{
			this.resistance -= hits;
		}

		public isDestroyed(): boolean
		{
			return this.resistance <= 0;
		}

		public reset(): void
		{
			this.resistance = DEFAULT_RESISTANCE;
		}

		public getCoordinates(): Coordinates
		{
			return this.coordinates;
		}

		public getColor(): ZipLedColors
		{
			return this.color;
		}
	}
}
