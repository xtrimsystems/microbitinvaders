namespace Bonuses
{

	export class Bonus
	{
		private coordinates: Coordinates;
		private readonly color: ZipLedColors;
		private readonly speed: number;

		public constructor(coordinates: Coordinates, color: ZipLedColors = ZipLedColors.Indigo, speed: number = 10)
		{
			this.coordinates = coordinates;
			this.color = color;
			this.speed = speed;
		}

		public move(): void
		{
			let x = this.coordinates.getX();
			let y = this.coordinates.getY();

			this.coordinates = new Coordinates(x, y + 1);
		}

		public getCoordinates(): Coordinates
		{
			return this.coordinates;
		}

		public getSpeed(): number
		{
			return this.speed;
		}

		public getColor(): ZipLedColors
		{
			return this.color;
		}
	}

	export class BonusController
	{
		private bonus: Bonus | undefined;
		private activeBonus: boolean;
		private readonly DISPLAY_WIDTH: number;
		private readonly DISPLAY_HEIGHT: number;

		public constructor(maxX: number, maxY: number) {
			this.DISPLAY_WIDTH = maxX;
			this.DISPLAY_HEIGHT = maxY;
			this.activeBonus = false;
		}

		public spawnBonus(): void
		{
			if (this.bonus === undefined && !this.activeBonus) {

				const randomNumber = Math.floor(Math.random() * 700);

				if (randomNumber === 7) {
					this.bonus = new Bonus(new Coordinates(Math.round(Math.random() * this.DISPLAY_WIDTH), 0));
				}
			}
		}

		public getBonus(): Bonus | undefined
		{
			return this.bonus;
		}

		public moveBonus(frames: number): void
		{
			if (this.bonus) {

				if (frames % this.bonus.getSpeed()) {
					return;
				}

				this.bonus.move();

				if (this.bonus.getCoordinates().getY() > this.DISPLAY_HEIGHT) {
					this.reset();
				}
			}

		}

		public reset(): void
		{
			this.bonus = undefined;
			this.activeBonus = false;
		}

		public activateBonus(): void
		{
			this.activeBonus = true;
			control.inBackground(() => {
				basic.pause(7000);
				this.activeBonus = false
			});
		}

		public isBonusActive(): boolean
		{
			return this.activeBonus;
		}
	}
}
