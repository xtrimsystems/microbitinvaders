namespace Enemies
{
	import Coordinates = Helpers.Coordinates;

	export class Enemy
	{
		private resistance: number;
		private coordinates: Coordinates;
		private readonly color: ZipLedColors;
		private readonly speed: number;

		public constructor(
			coordinates: Coordinates,
			color: ZipLedColors = ZipLedColors.Yellow,
			speed: number = 20,
			resistance: number = 1,
		) {
			this.coordinates = coordinates;
			this.color = color;
			this.speed = speed;
			this.resistance = resistance;
		}

		public move(): void
		{
			let x = this.coordinates.getX();
			let y = this.coordinates.getY();

			this.coordinates = new Coordinates(x, y + 1);
		}

		public getCoordinates(): Coordinates
		{
			return this.coordinates;
		}

		public getSpeed(): number
		{
			return this.speed;
		}

		public getColor(): ZipLedColors
		{
			return this.color;
		}

		public isDestroyed(): boolean
		{
			return this.resistance <= 0;
		}

		public receiveHit(hits: number): void
		{
			this.resistance -= hits;
		}
	}

	export class Swarm
	{
		private resistance: number;
		private coordinates: Coordinates;
		private readonly color: ZipLedColors;
		private readonly speed: number;
		private swarm: Enemy[];

		public constructor(
			coordinates: Coordinates,
			size: {width: number, height: number},
			minX: number,
			maxX: number,
			color: ZipLedColors = ZipLedColors.Yellow,
			speed: number = 20,
			resistance: number = 1,
		) {
			this.coordinates = coordinates;
			this.color = color;
			this.speed = speed;
			this.resistance = resistance;
			this.swarm = [];
			this.generateSwarm(size, minX, maxX);
		}

		private generateSwarm(size: {width: number, height: number}, minX: number, maxX: number)
		{
			for (let i = 0; i < size.height; i++) {
				for (let j = 0; j < size.width; j++) {

					const initialPoint = this.coordinates.getX();
					const x = j === 0 && i === 0 ? initialPoint : initialPoint + j > minX &&
					initialPoint + j <= maxX ? initialPoint + j : undefined;

					if (x) {
						this.swarm.push(new Enemy(new Coordinates(x, i)));
					}
				}
			}
		}

		public getEnemies(): Enemy[]
		{
			return this.swarm;
		}

		public setSwarm(swarm: Enemy[]): void
		{
			this.swarm = swarm;
		}

		public move(): void {

			let x = this.coordinates.getX();
			let y = this.coordinates.getY();

			this.coordinates = new Coordinates(x, y + 1);

			this.swarm.forEach((enemy) => enemy.move());
		}

		public removeOutOfBoundsEnemiesInSwarm(DISPLAY_HEIGHT: number): void
		{
			this.setSwarm(
				this.getEnemies().filter(
					(swarmEnemy) => swarmEnemy.getCoordinates().getY() <= DISPLAY_HEIGHT
				)
			);
		}

		public removeDestroyedEnemiesInSwarm(): void
		{
			this.setSwarm(this.getEnemies().filter((swarmEnemy: Enemy) => !swarmEnemy.isDestroyed()));
			if (this.swarm.length === 0) {
				this.resistance = 0;
			}
		}
		public getCoordinates(): Coordinates
		{
			return this.coordinates;
		}

		public getSpeed(): number
		{
			return this.speed;
		}

		public getColor(): ZipLedColors
		{
			return this.color;
		}

		public isDestroyed(): boolean
		{
			return this.resistance <= 0;
		}

		public receiveHit(hits: number): void
		{
			this.resistance -= hits;
		}
	}

	export class EnemyController
	{
		private enemies: Enemy[];
		private swarms: Swarm[];
		private readonly DISPLAY_WIDTH: number;
		private readonly DISPLAY_HEIGHT: number;

		public constructor(maxX: number, maxY: number)
		{
			this.enemies = [];
			this.swarms = [];
			this.DISPLAY_WIDTH = maxX;
			this.DISPLAY_HEIGHT = maxY;
		}

		public spawnEnemy(stage: number): void
		{
			if (this.enemies.length < stage) {
				if (stage < 5) {
					this.spawnSimpleEnemy();
				} else {
					if (Math.floor(Math.random() * 3) === 0) {
						this.spawnHeavyEnemy();
					} else {
						this.spawnSimpleEnemy();
					}
				}
			}
		}

		private spawnSimpleEnemy(): void
		{
			this.enemies.push(
				new Enemy(new Coordinates(Math.round(Math.random() * this.DISPLAY_WIDTH), 0))
			);
		}

		private spawnHeavyEnemy(): void
		{
			this.enemies.push(
				new Enemy(new Coordinates(Math.round(Math.random() * this.DISPLAY_WIDTH), 0), ZipLedColors.Red, 30, 2)
			)
		}

		private spawnEnemySwarm(): void
		{
			this.swarms.push(
				new Swarm(
					new Coordinates(Math.round(Math.random() * this.DISPLAY_WIDTH), 0),
					{ width: 3, height: 2 },
					0,
					this.DISPLAY_WIDTH
				)
			);
		}

		public moveEnemies(frames: number): number
		{
			this.enemies.forEach((enemy) => {
				if (frames % enemy.getSpeed()) {
					return;
				}
				enemy.move();
			});

			this.swarms.forEach((enemy) => {
				if (frames % enemy.getSpeed()) {
					return;
				}
				enemy.move();
			});

			const enemiesBefore = this.getTotalAmountOfEnemiesInPlay();

			this.enemies = this.enemies.filter((enemy) =>  enemy.getCoordinates().getY() <= this.DISPLAY_HEIGHT);
			this.swarms = this.swarms.filter((swarm) =>  {
				swarm.removeOutOfBoundsEnemiesInSwarm(this.DISPLAY_HEIGHT);
				return swarm.getCoordinates().getY() <= this.DISPLAY_HEIGHT
			});

			return enemiesBefore - this.getTotalAmountOfEnemiesInPlay();
		}

		public getEnemies(): Enemy[]
		{
			return this.enemies;
		}

		public getSwarms(): Swarm[]
		{
			return this.swarms;
		}

		public removeDestroyedEnemies(): number
		{
			const enemiesBefore = this.enemies.length;
			const swarmsBefore = this.swarms.length;
			const totalEnemiesBefore = enemiesBefore + swarmsBefore;

			this.enemies = this.enemies.filter((enemy) => !enemy.isDestroyed());

			this.swarms = this.swarms.filter((swarm) => {
				swarm.removeDestroyedEnemiesInSwarm();
				return !swarm.isDestroyed();
			});

			return totalEnemiesBefore - (this.enemies.length + this.swarms.length)
		}

		private getTotalAmountOfEnemiesInPlay(): number
		{
			let enemiesBefore = this.enemies.length;
			this.swarms.map((swarm) => enemiesBefore += swarm.getEnemies().length);

			return enemiesBefore;
		}

		public reset(): void
		{
			this.enemies = [];
			this.swarms = [];
		}
	}
}
