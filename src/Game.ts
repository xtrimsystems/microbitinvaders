namespace Game {

	import ZIP64ButtonEvents = GAME_ZIP64.ZIP64ButtonEvents;
	import ZIP64ButtonPins = GAME_ZIP64.ZIP64ButtonPins;

	import BulletController = Bullets.BulletController;
	import EnemyController = Enemies.EnemyController;
	import RenderEngine = Renderer.RenderEngine;
	import Spaceship = Player.Spaceship;
	import BonusController = Bonuses.BonusController;
	import CollisionsDetector = Helpers.CollisionsDetector;

	const enum GameState {
		ENDED,
		STARTED,
		PAUSED,
		PLAYER_KILLED,
		INTERNAL_PAUSED
	}

	export class Controller
	{
		private readonly bulletController: BulletController;
		private readonly enemyController: EnemyController;
		private readonly renderEngine: RenderEngine;
		private readonly playerSpaceShip: Spaceship;
		private readonly bonusController: BonusController;
		private gameState: GameState;
		private frames: number;
		private level: number;
		private score: number;
		private stage: number;
		private totalKills: number;
		private backgroundAnimation: boolean;

		public constructor(
			bulletController: BulletController,
			enemyController: EnemyController,
			renderEngine: RenderEngine,
			playerSpaceShip: Spaceship,
			bonusController: BonusController
		) {
			this.bulletController = bulletController;
			this.enemyController = enemyController;
			this.renderEngine = renderEngine;
			this.playerSpaceShip = playerSpaceShip;
			this.bonusController = bonusController;
			this.gameState = GameState.ENDED;
			this.frames = 0;
			this.level = 0;
			this.score = -1;
			this.stage = 1;
			this.totalKills = 0;
			this.backgroundAnimation = false;
		}

		private setGame(): void
		{
			this.gameState = GameState.ENDED;
			this.frames = 0;
			this.level = 0;
			this.score = -1;
			this.stage = 1;
			this.totalKills = 0;
			this.backgroundAnimation = false;
			this.playerSpaceShip.reset();
			this.bulletController.reset();
			this.enemyController.reset();
		}

		public run(): void
		{
			switch (this.gameState) {
				case GameState.ENDED:
					this.showBackgroundMessage('Press Fire1 to start');
					this.renderEngine.showRainbow();
					this.listenToStartGame();
					break;
				case GameState.STARTED:
					this.showBackgroundMessage(this.stage.toString());
					this.moveElementsInScreen();
					break;
				case GameState.PAUSED:
					this.showBackgroundMessage('Game Paused, press  Fire1 to continue', 100);
					break;
				case GameState.PLAYER_KILLED:
					this.renderEngine.showRainbow();
					basic.showString(`GAME OVER - Score: ${this.score}  Stage: ${this.stage}`, 100);
					this.setGame();
					this.gameState = GameState.ENDED;
					break;
				case GameState.INTERNAL_PAUSED:
					break;
			}

			this.frames++;

			if (this.frames > 30000) {
				this.frames = 0;
			}
		}

		private showBackgroundMessage(message: string, interval: number = 50): void
		{
			if (!this.backgroundAnimation) {

				this.backgroundAnimation = true;
				control.inBackground(() => {
					basic.showString(message, interval);
					this.backgroundAnimation = false;
				});

			}
		}

		private listenToStartGame(): void
		{
			if (GAME_ZIP64.buttonIsPressed(ZIP64ButtonPins.Fire1)) {
				this.gameState = GameState.STARTED;
				basic.pause(100);
				this.addButtonListeners();
			}
		}

		private addButtonListeners(): void
		{
			GAME_ZIP64.onButtonPress(ZIP64ButtonPins.Left, ZIP64ButtonEvents.Down, () => this.movePlayerLeft());
			GAME_ZIP64.onButtonPress(ZIP64ButtonPins.Right, ZIP64ButtonEvents.Down, () => this.movePlayerRight());
			GAME_ZIP64.onButtonPress(ZIP64ButtonPins.Fire2, ZIP64ButtonEvents.Down, () => this.shoot());
			GAME_ZIP64.onButtonPress(ZIP64ButtonPins.Fire1, ZIP64ButtonEvents.Down, () => this.togglePause());
		}

		private moveElementsInScreen(): void
		{
			this.moveBullets();
			this.moveEnemies();
			this.moveBonus();
			this.resolveCollisionBonusPlayer();
			this.bonusController.spawnBonus();
			this.enemyController.spawnEnemy(this.stage);
			this.renderState();
		}

		private moveBonus(): void
		{
			this.bonusController.moveBonus(this.frames);
			this.resolveCollisionBonusPlayer();
		}

		private moveBullets(): void
		{
			this.bulletController.moveBullets(this.frames);
			this.resolveCollisionsBulletsEnemies();
		}

		private moveEnemies(): void
		{
			const enemiesReachedBottom = this.enemyController.moveEnemies(this.frames);

			if (enemiesReachedBottom > 0) {
				this.resolveHitsToPlayer(enemiesReachedBottom);
			}
			this.resolveCollisionsBulletsEnemies();
		}

		private resolveHitsToPlayer(enemiesReachedBottom: number): void
		{
			this.playerSpaceShip.receiveHit(enemiesReachedBottom);
			GAME_ZIP64.runMotor(15);

			if (this.playerSpaceShip.isDestroyed()) {
				this.gameState = GameState.PLAYER_KILLED
			}
		}

		private resolveCollisionBonusPlayer(): void
		{
			if (this.bonusController.isBonusActive()) {
				this.bulletController.activateSpecialShoot();
			} else {
				this.bulletController.deactivateSpecialShoot();
			}

			const bonus = this.bonusController.getBonus();
			if (bonus !== undefined) {
				if(CollisionsDetector.isBonusInCollisionWithPlayer(bonus, this.playerSpaceShip)) {
					this.bonusController.reset();
					this.bonusController.activateBonus();
				}
			}
		}

		private resolveCollisionsBulletsEnemies(): void
		{
			CollisionsDetector.checkCollisionsBulletsEnemies(
				this.bulletController.getBullets(),
				this.enemyController.getEnemies(),
				this.enemyController.getSwarms()
			);

			const hits = this.bulletController.removeDestroyedBullets();
			const kills = this.enemyController.removeDestroyedEnemies();

			if(hits) {
				this.upgradeScore(hits, kills);
			}
		}

		private upgradeScore(hits: number, kills: number): void
		{
			this.score += hits;
			this.totalKills += kills;

			if (this.totalKills !== 1 && this.totalKills % this.stage === 0) {
				this.level++;
			}

			if (this.level > 9) {
				this.level = 0;
				this.stage++;
				this.gameState = GameState.INTERNAL_PAUSED;
				this.renderEngine.showRainbow();
				GAME_ZIP64.runMotor(15);
				basic.showString(`Next Stage: ${this.stage}`, 50);
				this.renderState();
				this.gameState = GameState.STARTED;
			}
		}

		private renderState(): void
		{
			this.renderEngine.renderState(
				this.playerSpaceShip,
				this.bulletController.getBullets(),
				this.enemyController.getEnemies(),
				this.enemyController.getSwarms(),
				this.bonusController.getBonus()
			);
		}

		private movePlayerLeft(): void
		{
			if (this.gameState === GameState.STARTED && this.playerSpaceShip.moveLeft(0)) {
				this.renderState();
			}
		}

		private movePlayerRight(): void
		{
			if (this.gameState === GameState.STARTED && this.playerSpaceShip.moveRight(DISPLAY_WIDTH)) {
				this.renderState();
			}
		}

		private shoot(): void
		{
			if (this.gameState === GameState.STARTED) {
				this.bulletController.shoot(this.playerSpaceShip.getCoordinates(), this.stage);
			}
		}

		private togglePause(): void
		{
			if (~[GameState.STARTED, GameState.PAUSED].indexOf(this.gameState)) {
				if (this.gameState === GameState.STARTED) {
					this.gameState = GameState.PAUSED;
				} else if (this.gameState === GameState.PAUSED) {
					this.gameState = GameState.STARTED;
				}
			}
		}
	}
}
