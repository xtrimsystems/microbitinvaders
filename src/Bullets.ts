namespace Bullets {

	import Coordinates = Helpers.Coordinates;

	export enum BulletDirection
	{
		Right,
		Left,
		Forward,
		Backward // Shoots from enemies ?
	}

	export class Bullet
	{
		private coordinates: Coordinates;
		private readonly color: ZipLedColors;
		private readonly direction: BulletDirection;
		private readonly speed: number;
		private destroyed: boolean;

		public constructor(
			coordinates: Coordinates,
			direction: BulletDirection,
			color: ZipLedColors = ZipLedColors.Purple,
			speed: number = 3
		) {
			this.coordinates = coordinates;
			this.direction = direction;
			this.color = color;
			this.speed = speed;
			this.destroyed = false;
		}

		public getCoordinates(): Coordinates
		{
			return this.coordinates;
		}

		public getColor (): ZipLedColors
		{
			return this.color;
		}

		public getSpeed(): number
		{
			return this.speed;
		}

		public move()
		{
			let x = this.coordinates.getX();
			let y = this.coordinates.getY();

			if (this.direction === BulletDirection.Left) {
				x -= 0.5;
			} else if (this.direction === BulletDirection.Right) {
				x += 0.5;
			}

			this.coordinates = new Coordinates(x , y -1);
		}

		public destroy()
		{
			this.destroyed = true;
		}

		public isDestroyed()
		{
			return this.destroyed;
		}
	}

	export class BulletController
	{
		private specialShoot: boolean;
		private bulletsOnPlay: Bullet[];
		private readonly DISPLAY_WIDTH: number;
		private readonly DISPLAY_HEIGHT: number;

		public constructor(maxX: number, maxY: number)
		{
			this.DISPLAY_WIDTH = maxX;
			this.DISPLAY_HEIGHT = maxY;
			this.specialShoot = false;
			this.bulletsOnPlay = [];
		}

		public shoot(coordinates: Coordinates, stage: number): void
		{
			if (stage < 3) {
				if(this.bulletsOnPlay.length === 0) {
					this.spawnBullet(coordinates);
				}
			} else if (stage < 7) {
				if(this.bulletsOnPlay.length < 2) {
					this.spawnBullet(coordinates);
				}
			} else if (stage < 10) {
				if(this.bulletsOnPlay.length < 3) {
					this.spawnBullet(coordinates);
				}
			} else if (stage >= 10) {
				if(this.bulletsOnPlay.length < 4) {
					this.spawnBullet(coordinates);
				}
			}
		}

		private spawnBullet(coordinates: Coordinates): void
		{
			if (this.isSpecialShoot()) {
				this.bulletsOnPlay.push(new Bullet(coordinates, BulletDirection.Forward));
				this.bulletsOnPlay.push(new Bullet(coordinates, BulletDirection.Left));
				this.bulletsOnPlay.push(new Bullet(coordinates, BulletDirection.Right));
			} else {
				this.bulletsOnPlay.push(new Bullet(coordinates, BulletDirection.Forward))
			}
		}

		public moveBullets(frames: number): void
		{
			this.bulletsOnPlay.forEach((bullet) => {
				if (frames % bullet.getSpeed()) {
					return;
				}

				bullet.move();
			});

			this.bulletsOnPlay = this.bulletsOnPlay.filter((bullet) =>
				bullet.getCoordinates().getY() >= 0 &&
				bullet.getCoordinates().getX() >= 0 &&
				bullet.getCoordinates().getY() <= this.DISPLAY_HEIGHT &&
				bullet.getCoordinates().getX() <= this.DISPLAY_WIDTH
			);
		}

		public getBullets(): Bullet[]
		{
			return this.bulletsOnPlay;
		}

		public removeDestroyedBullets(): number
		{
			const bulletsBefore = this.bulletsOnPlay.length;

			this.bulletsOnPlay = this.bulletsOnPlay.filter((bullet) => !bullet.isDestroyed());

			return bulletsBefore - this.bulletsOnPlay.length;
		}

		public isSpecialShoot(): boolean
		{
			return this.specialShoot;
		}

		public activateSpecialShoot()
		{
			this.specialShoot = true;
		}

		public deactivateSpecialShoot()
		{
			this.specialShoot = false;
		}

		public reset(): void
		{
			this.bulletsOnPlay = [];
			this.deactivateSpecialShoot();
		}
	}
}
