namespace Helpers
{
	import Enemy = Enemies.Enemy;
	import Swarm = Enemies.Swarm;
	import Bullet = Bullets.Bullet;
	import Bonus = Bonuses.Bonus;

	export 	class CollisionsDetector
	{
		public static isBonusInCollisionWithPlayer(bonus: Bonus, player: Spaceship): boolean
		{
			return bonus.getCoordinates().getX() === player.getCoordinates().getX() &&
				bonus.getCoordinates().getY() === player.getCoordinates().getY();
		}

		public static checkCollisionsBulletsEnemies(bullets: Bullet[], enemies: Enemy[], swarms: Swarm[]) {
			bullets.forEach((bullet) => {

				if(bullet.isDestroyed()) {
					return;
				}

				enemies.forEach((enemy) => {

					if(enemy.isDestroyed()) {
						return;
					}

					CollisionsDetector.resolveCollisionsWithEnemy(bullet, enemy);
				});

				swarms.forEach((enemy) => {

					if(enemy.isDestroyed()) {
						return;
					}

					CollisionsDetector.resolveCollisionsWithEnemySwarm(bullet, enemy);
				});
			});
		}

		private static resolveCollisionsWithEnemySwarm(bullet: Bullet, swarm: Swarm)
		{
			if(bullet.isDestroyed()) {
				return;
			}

			const enemies = swarm.getEnemies();

			enemies.forEach((enemy) => {

				if(enemy.isDestroyed()) {
					return;
				}

				CollisionsDetector.resolveCollisionsWithEnemy(bullet, enemy);
			});
		}

		private static resolveCollisionsWithEnemy(bullet: Bullet, enemy: Enemy)
		{
			if (CollisionsDetector.isBulletInCollisionWithEnemy(bullet, enemy)) {
				bullet.destroy();
				enemy.receiveHit(1);
			}
		}

		private static isBulletInCollisionWithEnemy(bullet: Bullet, enemy: Enemy): boolean
		{
			return bullet.getCoordinates().getX() === enemy.getCoordinates().getX() &&
				bullet.getCoordinates().getY() === enemy.getCoordinates().getY();
		}
	}

	export class Coordinates
	{
		private readonly x: number;
		private readonly y: number;

		public constructor(x: number, y: number) {
			this.x = x;
			this.y = y;
		}

		public getX(): number
		{
			return this.x;
		}

		public getY(): number
		{
			return this.y;
		}
	}
}
