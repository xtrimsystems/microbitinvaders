namespace Renderer
{
	import ZIP64Display = GAME_ZIP64.ZIP64Display;

	import Enemy = Enemies.Enemy;
	import Swarm = Enemies.Swarm;
	import Spaceship = Player.Spaceship;
	import Bullet = Bullets.Bullet;
	import Bonus = Bonuses.Bonus;

	export class RenderEngine
	{
		private display: ZIP64Display;

		public constructor(display: ZIP64Display)
		{
			this.display = display;
		}

		public renderState(player: Spaceship, bullets: Bullet[], enemies: Enemy[], swarms: Swarm[], bonus?: Bonus)
		{
			this.display.clear();
			this.renderPlayer(player);
			this.renderBullets(bullets);
			this.renderEnemies(enemies);
			this.renderSwarms(swarms);
			if (bonus) {
				this.renderBonus(bonus);
			}
			this.display.show();
		}

		public showRainbow(): void
		{
			this.display.clear();
			this.display.showRainbow();
		}

		private renderPlayer(player: Spaceship): void
		{
			const coordinates = player.getCoordinates();
			this.display.setMatrixColor(coordinates.getX(), coordinates.getY(), player.getColor());
		}

		private renderBullets(bullets: Bullet[]): void
		{
			bullets.forEach((bullet) => {
				const x = bullet.getCoordinates().getX() < 4 ? Math.floor(bullet.getCoordinates().getX()) : Math.ceil(bullet.getCoordinates().getX());
				this.display.setMatrixColor(x, bullet.getCoordinates().getY(), bullet.getColor());
			});
		}

		private renderEnemies(enemies: Enemy[]): void
		{
			enemies.forEach((enemy) =>  {
				this.display.setMatrixColor(enemy.getCoordinates().getX(), enemy.getCoordinates().getY(), enemy.getColor());
			});
		}

		private renderSwarms(enemies: Swarm[]): void
		{
			enemies.forEach((enemy: Swarm) =>
				enemy.getEnemies().forEach((swarm) =>
					this.display.setMatrixColor(swarm.getCoordinates().getX(), swarm.getCoordinates().getY(), swarm.getColor())
				)
			);
		}

		private renderBonus(bonus: Bonus): void
		{
			const coordinates = bonus.getCoordinates();
			this.display.setMatrixColor(coordinates.getX(), coordinates.getY(), bonus.getColor());
		}
	}
}
