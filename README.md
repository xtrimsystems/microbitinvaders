# Microbit: Invaders

To compile run: `pxt`

If microbit is connected via usb and gets detected, it will compile directly,
if not copy the binary.hex file manually into the microbit root folder.

## Debug
###Linux
- Install the program `screen` if it is not already installed.
- Plug in the micro:bit.
- Open a terminal.
- Find which device node the micro:bit was assigned to with the command `ls /dev/ttyACM*`.
- If it was `/dev/ttyACM0`, type the command `screen /dev/ttyACM0 115200`. 
If it was some other device node, use that one in the command instead. 
_Note_: You may need root access to run screen successfully. 
You can probably use the command  sudo like this: `sudo screen /dev/ttyACM0 115200`.
- To exit screen, type Ctrl-A Ctrl-D.

Alternative programs include minicom and so on.
