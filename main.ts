import ZIP64Display = GAME_ZIP64.ZIP64Display;

import EnemyController = Enemies.EnemyController;
import GameController = Game.Controller;
import Coordinates = Helpers.Coordinates;
import Spaceship = Player.Spaceship;
import RenderEngine = Renderer.RenderEngine;
import BulletController = Bullets.BulletController;
import BonusController = Bonuses.BonusController;

const DISPLAY_WIDTH = 7;
const DISPLAY_HEIGHT = 7;
const CENTER_SCREEN_X_AXIS = 3;
const BRIGHTNESS = 5;

const zip64Display: ZIP64Display = GAME_ZIP64.createZIP64Display();
zip64Display.setBrightness(BRIGHTNESS);

const gameController = new GameController(
	new BulletController(DISPLAY_WIDTH, DISPLAY_HEIGHT),
	new EnemyController(DISPLAY_WIDTH, DISPLAY_HEIGHT),
	new RenderEngine(zip64Display),
	new Spaceship(new Coordinates(CENTER_SCREEN_X_AXIS, DISPLAY_HEIGHT)),
	new BonusController(DISPLAY_WIDTH, DISPLAY_HEIGHT)
);

basic.forever(() => {
	gameController.run();
	basic.pause(0.06);
});
